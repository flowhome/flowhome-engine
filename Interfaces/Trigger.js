import { Observable, State } from './Observable'
import { Data } from './Data'
import { Logger } from '../Modules/Logger'

export class Trigger extends Observable{
    constructor() { Logger.Instance.debug("Trigger:Constructor")
        super()
        this._payload = null
        this._enabled = false
    }

    emit() { Logger.Instance.debug("Trigger:Emit")
        if (typeof this._context === 'undefined') throw new Error(`No context specified ${typeof payload}`)

        this._state = State.Running

        if (!this._payload) {
            this._payload = new Data()
            this._context.set("processCost", 0)
        }

        let error
        this._subscribers.forEach(subscriber => {

            this._context.set("processCost",this._context.get("processCost")+1)
            try {
                error = subscriber.next(this._subscribers.length >= 2 ? this._payload.clone : this._payload)
            } catch (error) {
                error = error
            }
        })

        if (!error) this._state = State.Waiting
        else {
            this._state = State.Failed
            Logger.Instance.error(error)
        }
    }

    next(payload) { Logger.Instance.debug(`Trigger:Next - ${JSON.stringify(payload)}`)
        if (!payload instanceof Data) throw new TypeError(`Expected Data got ${typeof payload}`)
        
        this._payload = payload
    }

    enable() { Logger.Instance.debug("Trigger:Enable")
        this._enabled = true
    }

    disable() { Logger.Instance.debug("Trigger:Disable")
        this._enabled = false
    }
}