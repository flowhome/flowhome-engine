import { Observable } from './Observable'
import { Data } from './Data';
import { Logger } from '../Modules/Logger'

export class Filter extends Observable {
    constructor(constraint) { Logger.Instance.debug(`Filter:Constructor - ${JSON.stringify(constraint)}`)
        if (!constraint instanceof Object) throw new TypeError(`Expected Object got ${typeof constraint}`)

        super()
        this._constraint = constraint
    }

    next(payload) { Logger.Instance.debug(`Filter:Next - ${JSON.stringify(payload)}`)
        if (!payload instanceof Data) throw new TypeError(`Expected Data got ${typeof payload}`)
        if (typeof this._context === 'undefined') throw new Error(`No context specified ${typeof payload}`)

        this._state = State.Running

        let error
        if (this.constraint(payload)) {
            this._state = State.Cancled
            return
        }
        this._subscribers.forEach(subscriber => {
            this._context.set("processCost",this._context.get("processCost")+1)
            try {
                error = subscriber.next(this._subscribers.length >= 2 ? payload.clone : payload)
            } catch (error) {
                error = error
            }
        })

        if (!error) this._state = State.Waiting
        else {
            this._state = State.Failed
            return error
        }
    }
}