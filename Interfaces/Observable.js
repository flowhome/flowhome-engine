import { Data } from './Data'
import { Logger } from '../Modules/Logger'
export class Observable {
    constructor() { Logger.Instance.debug("Observable:Constructor")
        this._guid = Math.random().toString(36).substring(2, 15)
        this._state = State.Waiting
        this._subscribers = []
    }

    get guid() { Logger.Instance.debug("Observable:[GET]Guid")
        return this._guid
    }

    get context() { Logger.Instance.debug("Observable:[GET]Context")
        return this._context
    }

    set context(context) { Logger.Instance.debug(`Observable:[SET]Context - ${JSON.stringify(context)}`)
        if (!context instanceof Data) throw new TypeError(`Expected Data got ${typeof context}`)

        this._context = context
        this._subscribers.forEach(subscriber => {
            this._context.set("estimatedCost",this._context.get("estimatedCost")+1)
            subscriber.context = this.context
        })
    }

    get state() { Logger.Instance.debug("Observable:[GET]State")
        return this._state
    }

    subscribe(observable) { Logger.Instance.debug(`Observable:Subscribe - ${JSON.stringify(observable)}`)
        if (!observable instanceof Observable) throw new TypeError(`Expected Observable got ${typeof observable}`)

        this._subscribers.push(observable)
    }

    next(payload) { Logger.Instance.debug(`Observable:Next - ${JSON.stringify(payload)}`)
        if (!payload instanceof Data) throw new TypeError(`Expected Data got ${typeof payload}`)
        if (typeof this._context === 'undefined') throw new Error(`No context specified ${typeof payload}`)

        this._state = State.Running

        let error
        this._subscribers.forEach(subscriber => {

            this._context.set("processCost",this._context.get("processCost")+1)
            try {
                error = subscriber.next(this._subscribers.length >= 2 ? payload.clone : payload)
            } catch (error) {
                error = error
            }
        })

        if (!error) this._state = State.Waiting
        else {
            this._state = State.Failed
            return error
        }
    }
}

export const State = {
    Waiting: 'Waiting',
    Running: 'Running',
    Failed: 'Failed',
    Cancled: 'Cancled'
}