import { Trigger } from './Trigger'
import { CronJob } from 'cron'
import { Logger } from '../Modules/Logger'

export class TimeTrigger extends Trigger {
    constructor (expression, timeZone = 'Europe/Berlin') { Logger.Instance.debug(`TimeTrigger:Constructor - ${JSON.stringify(expression)} - ${JSON.stringify(timeZone)}`)
        if (!expression instanceof String) throw new TypeError(`Expected String got ${typeof expression}`)
        if (!timeZone instanceof String) throw new TypeError(`Expected String got ${typeof timeZone}`)

        super() 
        this.task = new CronJob(expression, this.emit, null, this._enabled, timeZone, this, false)
    }

    enable () { Logger.Instance.debug("TimeTrigger:Enable")
        super.enable()
        this.task.start()
    }

    disable () { Logger.Instance.debug("TimeTrigger:Disable")
        super.disable()
        this.task.start()
    }
}