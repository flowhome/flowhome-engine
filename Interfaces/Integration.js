import { Data } from "./Data"
import { Logger } from '../Modules/Logger'

export class Integration {
    constructor(context) { Logger.Instance.debug(`Integration:Constructor - ${JSON.stringify(context)}`)
        if (!context instanceof Data) throw new TypeError(`Expected Interface got ${typeof context}`)
        
    }
}