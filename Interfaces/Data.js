import { Logger } from '../Modules/Logger'

export class Data {
    constructor(template = {}) { Logger.Instance.debug(`Data:Constructor - ${JSON.stringify(template)}`)
        if (!template instanceof Object) new TypeError(`Expected Object got ${typeof template}`)

        this._template = template
        this.reset()
    }

    get template() { Logger.Instance.debug("Data:[GET]Template")
        return this._template
    }

    reset() { Logger.Instance.debug("Data:Reset")
        this._data = this._template
    }

    get(namespace) { Logger.Instance.debug(`Data:Get - ${JSON.stringify(namespace)}`)
        if (!namespace instanceof String) throw new TypeError(`Expected String got ${typeof namespace}`)

        return this.flatten(this._data)[namespace]
    }

    set(namespace, value) { Logger.Instance.debug(`Data:Set - ${JSON.stringify(namespace)} - ${JSON.stringify(value)}`)
        if (!namespace instanceof String) throw new TypeError(`Expected String got ${typeof namespace}`)
        if (!value instanceof Object) throw new TypeError(`Expected Object got ${typeof value}`)

        let data = this.flatten(this._data)
        if (!data[namespace])
            Object.assign(this._data, this._data, this.expand({ [namespace]: value }))
        else {
            data[namespace] = value
            this._data = this.expand(data)
        }
    }

    get clone() { Logger.Instance.debug("Data:Clone")
        return new Data(Object.assign({}, this._data))
    }

    toString() {
        return JSON.stringify(this._data)
    }

    expand(data) { Logger.Instance.debug("Data:Expand")
        if (Object(data) !== data || Array.isArray(data))
            return data;
        var regex = /\.?([^.\[\]]+)|\[(\d+)\]/g,
            resultholder = {};
        for (var p in data) {
            var cur = resultholder,
                prop = "",
                m;
            while (m = regex.exec(p)) {
                cur = cur[prop] || (cur[prop] = (m[2] ? [] : {}));
                prop = m[2] || m[1];
            }
            cur[prop] = data[p];
        }
        return resultholder[""] || resultholder;
    }
    
    flatten(data) { Logger.Instance.debug("Data:Flatten")
        try {
            var result = {};
            function recurse (cur, prop) {
                if (Object(cur) !== cur) {
                    result[prop] = cur;
                } else if (Array.isArray(cur)) {
                    for(var i=0, l=cur.length; i<l; i++)
                        recurse(cur[i], prop + "[" + i + "]");
                    if (l == 0)
                        result[prop] = [];
                } else {
                    var isEmpty = true;
                    for (var p in cur) {
                        isEmpty = false;
                        recurse(cur[p], prop ? prop+"."+p : p);
                    }
                    if (isEmpty && prop)
                        result[prop] = {};
                }
            }
            recurse(data, "");
            return result;
        } catch (error) {
            console.log(error)
        }
    }
}