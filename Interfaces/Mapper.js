import { Observable, State } from './Observable'
import { Integration } from './Integration'
import { Data } from './Data'
import { Logger } from '../Modules/Logger'

export class Mapper extends Observable {
    constructor(integration) { Logger.Instance.debug(`Mapper:Constructor - ${JSON.stringify(integration)}`)
        if (!integration instanceof Integration) throw new TypeError(`Expected Interface got ${typeof integration}`)

        super()
        this._integration = integration
    }

    next(payload) { Logger.Instance.debug(`Mapper:Next - ${JSON.stringify(payload)}`)
        if (!payload instanceof Data) throw new TypeError(`Expected Data got ${typeof payload}`)
        if (typeof this._context === 'undefined') throw new Error(`No context specified ${typeof payload}`)

        this._state = State.Running
        
        let error
        
        this._integration.execute(payload)    

        this._subscribers.forEach(subscriber => {
            this._context.set("processCost",this._context.get("processCost")+1)
            try {
                error = subscriber.next(this._subscribers.length >= 2 ? payload.clone : payload)
            } catch (error) {
                error = error
            }
        })

        if (!error) this._state = State.Waiting
        else {
            this._state = State.Failed
            return error
        }
    }
}