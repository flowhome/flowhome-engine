import { Integration } from '../Interfaces/Integration'
import { Data } from '../Interfaces/Data'
import { Logger } from '../Modules/Logger'

export class ConsoleLog extends Integration {
    constructor(context) { Logger.Instance.debug("ConsoleLog:Constructor")
        if (!context instanceof Data) throw new TypeError(`Expected Data got ${typeof context}`)

        super(context)        
    }

    execute(payload) { Logger.Instance.debug(`ConsoleLog:Execute - ${JSON.stringify(payload)}`)
        if (!payload instanceof Data) throw new TypeError(`Expected Data got ${typeof payload}`)
        payload.set("test.test", "Deine Mama")
        payload.set("spotify.account.accesstoken", "deine mutter stinkt")
        console.log(JSON.stringify(payload))
    }
}