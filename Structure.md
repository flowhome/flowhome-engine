Observable
    constructor()
    guid
        get()
    context
        get()
        set(Context)
    subscribe(Observable)
    next(Data)
    state
        get()
        set(State)

Trigger extends Observable
    emit(Data)
    enable()
    disable()
    optional next(Data)

Mapper extends Observable
    constructor(Interface)
    next(Data)

Filter extends Observable
    constructor(constraints)
    next(Data)

Interface
    constructor(...props)
    execute(Data)
    template
        get()

Data
    constructor()
    get(namespace)
    set(namespace, data)