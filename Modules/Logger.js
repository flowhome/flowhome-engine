import path from 'path'
import fs from 'fs'

import Winston from 'winston'
import 'winston-daily-rotate-file'
import { Mail } from 'winston-mail'
import Sentry from 'winston-transport-sentry'

export class Logger {
    constructor () {
        if (!fs.existsSync(path.join(path.dirname(require.main.filename), "Logs"))) fs.mkdirSync(path.join(path.dirname(require.main.filename), "Logs"))        
    }

    createLogger() {
        const { combine, timestamp, printf } = Winston.format
        return this._logger = Winston.createLogger({
            level: 'debug',
            exitOnError: false,
            format: combine(
                timestamp({
                    format: "YYYY-MM-DD HH:mm:ss"
                }),
                printf(info => {
                    if (info.meta && info.meta instanceof Error) {
                        return `[${info.timestamp}][${info.level}] >> ${info.message} : ${info.meta.stack}`;
                      }
                    return `[${info.timestamp}][${info.level}] >> ${info.message}`;
                })
            ),
            transports: [
                new Winston.transports.Console({
                    handleExceptions: true,
                    colorize: true
                }),
                new Winston.transports.DailyRotateFile({
                    filename: 'Error.%DATE%.log',
                    dirname: path.join(path.dirname(require.main.filename), "Logs"),
                    datePattern: 'YYYY-MM-DD-HH',
                    maxSize: '100m',
                    maxFiles: 5,
                    zippedArchive: true, 
                    level: 'error',
                    colorize: false
                }),
                new Winston.transports.DailyRotateFile({
                    filename: 'Combined.%DATE%.log',
                    dirname: path.join(path.dirname(require.main.filename), "Logs"),
                    datePattern: 'YYYY-MM-DD-HH',
                    maxSize: '100m',
                    maxFiles: 5,
                    level: 'debug',
                    zippedArchive: true,
                    colorize: false
                }),
                new Sentry({
                    level: 'warn',
                    dsn: "https://51bac059973a489a87ccb4ea48aa4814@sentry.io/1495142",
                    tags: { key: 'value' },
                    extra: { key: 'value' },
                    patchGlobal: true
                })
            ]
        })
    }

    static get Instance() {
        if (!Logger._instance)
            Logger._instance = new Logger().createLogger()
        return Logger._instance
    }
}
