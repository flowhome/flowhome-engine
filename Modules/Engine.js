import { TimeTrigger } from '../Interfaces/TimeTrigger'
import { Observable } from '../Interfaces/Observable'
import { Mapper } from '../Interfaces/Mapper'
import { ConsoleLog } from '../Integrations/ConsoleLog'
import { Data } from '../Interfaces/Data';
import { Logger } from './Logger'

export class Engine {
    constructor() {
        process.on('uncaughtException', Logger.Instance.error)
    }
    run() {
        try {
            var emptyData = new Data()
            var consoleLog = new ConsoleLog(emptyData)

            var timer = new TimeTrigger("* * * * *")
            var splitter = new Observable()
            var mapper1 = new Mapper(consoleLog)
            var mapper2 = new Mapper(consoleLog)
            var logger = new Mapper(consoleLog)
        
        
            timer.subscribe(splitter)
            splitter.subscribe(mapper1)
            splitter.subscribe(mapper2)
            mapper1.subscribe(logger)
            mapper2.subscribe(logger)
        
            timer.context = new Data({
                processCost: 0,
                estimatedCost: 0
            })

            timer.enable()
            setInterval(() => {
                Logger.Instance.info(timer.context)
            },500)
        } catch (error) {
            Logger.Instance.error(error)
        }
    }
}

var engine = new Engine()
engine.run()
